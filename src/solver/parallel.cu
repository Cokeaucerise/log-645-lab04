#include <iostream>

#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "parallel.cuh"
#include "../matrix/matrix.hpp"

#define errCheck(code) { errorCheck((code), __FILE__, __LINE__); }
void addWithCuda(double ** matrix, int rows, int cols, int iterations, double td, double h_square);

using std::cout;
using std::flush;
using std::endl;

inline void errorCheck(cudaError_t code, const char* file, int line) {
	if(cudaSuccess != code) {
		std::cout << "[" << file << ", line " << line << "]" << std::flush;
		std::cout << " CUDA error <" << cudaGetErrorString(code) << "> received." << std::endl << std::flush;
		exit(EXIT_FAILURE);
	}
}

__device__ double calculate_p(double c, double l, double r, double t, double b, double td_over_h_square) {
	return c * (1.0 - 4.0 * td_over_h_square) + (t + b + l + r) * (td_over_h_square);
}

__global__ void row_kernel(double * dev_in, double * dev_out, double td_over_h_square, int rows, int cols) {
	extern __shared__ double shared[]; 	// Dynamic sized shared memory for blocks
	double c, l, r, t, b;

	//remove the border cell from the id computation
	int x = (blockDim.x - 2) * blockIdx.x + threadIdx.x;
	int y = (blockDim.y - 2) * blockIdx.y + threadIdx.y;

	if (y >= rows || x >= cols ) return; // Outside of the grid

	int grid_i = x + (y * cols);
	int block_i = threadIdx.x + (threadIdx.y * blockDim.x);

	// Check if thread is a border of a block
	bool is_block_border = x >= cols - 1 || y >= rows - 1 || 				// Blocks that overflow the grid
		threadIdx.x <= 0 || threadIdx.y <= 0 || 							// Lower bound
		threadIdx.x >= blockDim.x - 1 || threadIdx.y >= blockDim.y - 1;		// Upper bound

	//Load grid value to shared memory
	shared[block_i] = dev_in[grid_i];

	// Sync the threads inside the block to make sure all the values are loaded in the shared memory
	__syncthreads();

	// Ignore the border cells for the calculation
	if(is_block_border) return;
		
	// Load dependances from shared memory
	c = shared[block_i];
	l = shared[block_i - 1];
	r = shared[block_i + 1];
	t = shared[block_i - blockDim.x];
	b = shared[block_i + blockDim.x];

	// Write the result directly in the dev_out
	dev_out[grid_i] = calculate_p(c, l, r, t, b, td_over_h_square);
}

void solvePar(int rows, int cols, int iterations, double td, double h, double ** matrix) {
	// Check if there's more rows than cols and swap if the difference is bigger than
	// the full value of the smallest dimension to avoid swaping near squared matrix
	bool transpose = cols < rows && (rows - cols) > min(cols, rows);
	int dev_rows = transpose ? cols : rows;
	int dev_cols = transpose ? rows : cols;

	double ** dev_matrix = transpose ? allocateMatrix(dev_rows, dev_cols) : matrix;

	if(transpose)
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) {
				(transpose ? dev_matrix[j][i] : dev_matrix[i][j]) = matrix[i][j];
			}
		}

	addWithCuda(dev_matrix, dev_rows, dev_cols, iterations, td, h*h);

	if(transpose)
		for(int i = 0; i < dev_rows; i++) {
			for(int j = 0; j < dev_cols; j++) {
				(transpose ? matrix[j][i] : matrix[i][j]) = dev_matrix[i][j];
			}
		}

	if(transpose) deallocateMatrix(dev_rows, dev_matrix);
}

void addWithCuda(double ** matrix, int rows, int cols, int iterations, double td, double h_square) {
	double * dev_a, * dev_b = nullptr;
	double * dev_out = new double[rows * cols];
	const double td_over_h_square = td / h_square;

	int nDevice = 0;
	cudaDeviceProp deviceProp;
	errCheck(cudaSetDevice(nDevice));
	errCheck(cudaGetDeviceProperties(&deviceProp, nDevice));

	const int max_t_per_b = deviceProp.maxThreadsPerBlock;

	errCheck(cudaMalloc(&dev_a, rows * cols * sizeof(double)));
	errCheck(cudaMalloc(&dev_b, rows * cols * sizeof(double)));
	for(int i = 0; i < rows; i++) errCheck(cudaMemcpy(&dev_a[i * cols], matrix[i], cols * sizeof(double), cudaMemcpyHostToDevice));

	// Compute the number of rows that can fit in the matrix X size. Min of 3 for dependances
	int block_rows 	= max((int)floor((double)max_t_per_b / (double)(cols)), 3); 
	int block_cols 	= floor((double)max_t_per_b / (double)block_rows);

	// Don't count the borders of the matrix and the borders of every blocks
	int grid_rows 	= ceil((double)(rows - 2) / (double)(block_rows - 2));
	int grid_cols 	= ceil((double)(cols - 2) / (double)(block_cols - 2));

	dim3 grid_size(grid_cols, grid_rows, 1);
	dim3 block_size(block_cols, block_rows, 1);
	const int shared_memory = block_size.x * block_size.y * sizeof(double);

	for(int k = 0; k < iterations; k++) {
		// Select witch pointer for in & out by switching them every iterations
		double * dev_in 	= k % 2 == 0 ? dev_a : dev_b;
		double * dev_out 	= k % 2 == 1 ? dev_a : dev_b;
		row_kernel<<<grid_size, block_size, shared_memory>>>(dev_in, dev_out, td_over_h_square, rows, cols);
	}

	// Get the correct dev_out by checking the total number of iterations
	double * final_out = iterations % 2 == 1 ? dev_b : dev_a;

	// Copy result, clean and end the CUDA execution
	errCheck(cudaGetLastError());
	errCheck(cudaDeviceSynchronize());
	errCheck(cudaMemcpy(dev_out, final_out, (rows) * (cols) * sizeof(double), cudaMemcpyDeviceToHost));
	errCheck(cudaFree(dev_a));
	errCheck(cudaDeviceReset());

	// Copy back the now computed matrix in the original pointer
	for(int i = 0; i < rows; i++) memcpy(matrix[i], &dev_out[i * cols], cols * sizeof(double));
}