Sun Dec  8 14:31:26 2019       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 440.26       Driver Version: 440.26       CUDA Version: 10.2     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  GeForce GTX 105...  Off  | 00000000:01:00.0 Off |                  N/A |
| N/A   34C    P8    N/A /  N/A |      0MiB /  4042MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   1  GeForce GTX 1080    Off  | 00000000:07:00.0  On |                  N/A |
| 49%   42C    P0    46W / 180W |    849MiB /  8119MiB |      3%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|    1      2120      G   /usr/lib/xorg/Xorg                           341MiB |
|    1      3983      G   compton                                        1MiB |
|    1      4087      G   /usr/lib/firefox/firefox                       1MiB |
|    1      4170      G   /usr/lib/firefox/firefox                       1MiB |
|    1      5697      G   /home/balthazar/.steam/ubuntu12_32/steam      43MiB |
|    1      5721      G   ./steamwebhelper                               2MiB |
|    1      5735      G   ...uest-channel-token=16984810223990724935   230MiB |
|    1      7146      G   ...uest-channel-token=12806467916089176244   197MiB |
|    1      8550      G   /usr/lib/firefox/firefox                       1MiB |
+-----------------------------------------------------------------------------+
