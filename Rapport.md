
<img src="./ETS-logo.png" style="display:inline-block;" width="200">

<h3 style="margin-top:-100px;text-align:right;">Département de génie logiciel et des TI</h3>

<h2 style="margin-top:100px;text-align:right;">Laboratoire 4 : Once Exploration de la librairie OpenCL/CUDA</h2>

|Cours		|LOG645 – Architectures de calculs parallèles	|
|:----------|----------------------------------------------:|
|Sessions	|Automne 2019									|
|Groupe		|01												|
|Étudiants	|Olivier Lamarre - LAMO13069505					|
|Date		|08/12/2019										|

<div style="page-break-after: always;"></div>

## Matériel utilisé

    OS: Ubuntu 19.04 disco
    Kernel: x86_64 Linux 5.0.0-37-generic
    CPU: Intel Core i7-8750H @ 12x 3.28GHz
    GPU: GeForce GTX 1080
    RAM: 3611MiB / 31781MiB

### Plateforme de calcule GPU

    NVIDIA Driver Version: 440.26
    CUDA Version: 10.2

## Introduction

Dans ce laboratoire, l'objectif est de faire usage à plus grande échelle de la parallélisation en prenant avantage d'un *Graphical Processing Unit* (*GPU*). Grâce à leur design spécifique qui vise à avoir le plus grand nombre de *processing unit* possible, ils peuvents exécuter une quantitée immense de calcules en simultané. Parcontre, il est plus compliqué d'écrire un algorithme pour ces platformes parce qu'il faut prendre en compte les limits physiques qu'ils ont.

## Temps d'exécution

### Résultats
    Test #1
    Command: ./lab4 4073 4021 32 0.0025 1 0 5 0 5
    Runtime sequential: 1.86 seconds
    Runtime parallel  : 0.57 seconds
    Acceleration      : 3.27
---
    Test #2
    Command: ./lab4 212 278 9865 0.0025 1 0 5 273 278
    Runtime sequential: 1.74 seconds
    Runtime parallel  : 0.30 seconds
    Acceleration      : 5.73
---
    Test #3
    Command: ./lab4 8367 26 3133 0.0025 1 8362 8367 0 5
    Runtime sequential: 2.34 seconds
    Runtime parallel  : 0.31 seconds
    Acceleration      : 7.62
---
    Test #4
    Command: ./lab4 9 9375 9500 0.0025 1 4 9 9370 9375
    Runtime sequential: 2.22 seconds
    Runtime parallel  : 0.29 seconds
    Acceleration      : 7.55

### Discussion

Les résultats obtenue sont intéressant parce qu'il ont une différence notable dans leur accélération poru chaque test. La meilleur performance est obtenue quand le nombre de rangé est minimale et le nombre d'itération est grand. Et la pire accélération est obtenue quand les dimensions sont égaleset le nombre d'itération est faible.

Alors, on peut en conclure rapidement que nous profitons le plus d'un grand nombre d'itération pour notre algorithme. Fort probablement parce qu'à chaque itération nous gagnons du temps sur l'exécution total.

Ensuite, Nous avons une bien meilleur performance quand le nombre de rangé est faible. Mais dans notre cas nous avons des performances similaire. C'est parce que nous effectuons une transposition de la matrice pour avoir le plus petit nombre de rangées avant de commancer la calcule.

<!-- Cela est explicable par le fait que les chargements de la mémoire se font plus *naturelement* en rangé qu'en colonne. Nous découpons la matrice en rangés avec le plus grand nombre de colonnes que nous pouvons avoir selons le matériel utilisé. Alors, quand nous avons de grandes rangé plustot que de longues colonnes nous pouvons charger des longs segments de mémoire pour chaque *block* d'exécution. Ce qui, au final, est grandement bénifique à la performance d'exécution sur le GPU. -->

## Questions

### Quelle est l’accélération de votre version parallèle relativement à votre version séquentielle?

Nous avons plusieurs type de résultats dépandament de la structure de la matrice et le nombre d'itération demandées.

- Dans le pire scénario nous avons une accélération de ~**3.25**.
- Dans le meilleur scénario nous avons une accélération de ~**7.60**.
- En moyenne nous avons une accélération de ~**6.05**


### Comment est-ce que les unités d’exécution communiquent-elles entre elles?

Chaques unités d’exécution (*thread*) ne communique pas directement entre eux. Qu'est-ce qui est fait pour pouvoir transmettre de l'information d'un *thread* à un autre est qu'ils ont tous accès à une mémoire partagé dans un même *block*. Alors, quand un *thread* à besoin d'une dépendance appartenant à un autre *thread* il peut la chercher directement la la plage de mémoire.

### Comment la communication se compare-t-elle avec MPI?

L'approche initial est semblable à cause qu'il faut concevoir une logiciel qui est exécuté sur tous les *threads* simultanémant. Mais, le grand avantage de OpenCL/CUDA est que nous avons une mémoire partagée. Cela est un avantage considérable parce que nous avons pas besoin d'explicitement communiquer les valeurs pertinantes aux récipiants désirés.

### Comment la communication se compare-t-elle avec OpenMP?

La plus grande similarité est que nous avons accès à une mémoire partagé, mais nous ne pouvons pas programmr avec la même abstraction d'avoir un seul programme. Il est nécessaire de concevoir un programme qui est concue pour être exécuté multiple fois.

### Pourquoi est-il nécessaire d’utiliser la fonction clCreateBuffer plutôt que de simplement passer un pointeur vers les données à la carte vidéo?

La mémoire utilisé n'est pas la même. Quand nous exécutons du code sur un GPU nous utilisons ses rssources. Cela veux dire sa mémoire, ses millier de processeurs et sa cache. Alors, avant de pouvoir lancer du code sur le GPU il faut allouer de la mémoire sur celui-ci.

## Conception de l'algorithme

### Conception

Nous avions **4** soucis principale durant la conception de notre algorithme.
1. Le découpage des *blocks*
2. Le découpage de la *grid*
3. La lecture de la mémoire dans le *Kernel*
4. L'écriture du résultat dans une matrice

En premier, nous avons choisi une approche qui est simple et peut-être même naïve. Nous avons choisie de privilégier la longeur des rangées des *blocks*. Alors, nous regardons combien de rangées nous pouvons avoir avec le nombre de colones dans la matrice, pour un minimum de 3 à causes des dépendances. 

Ensuite, le découpage de la *grid* est très simple. Nous calculons le nombre minimal de *block* qui est nécessaire pour couvrire toute la matrice. Par contre, une petite complexité suplémentaire est ajouter dans le calcule parce que nous devons ignorer les rangées et les colonnes qui sont sur les bordures des *blocks*. Nous fesons cela parce que ces *threads* ne calcules rien et servent juste à charger en mémoire leur valeur.

En plus, dans notre *Kernel* nous avons choisie de chargé en mémoire partagée la case qui correspond au *thread* qui l'exécute. Grâce à cela, nous pouvons avoir une grande homogénéité sur le temps d'exécution de chaque *Kernel* parce que nous fesons le même nombre de lecture sur la mémoire partagée pour chaque *Kernel*. Nous chargons aussi les bordures de chaque block en mémoire partagée, par contre nous ne feson aucun calcule avec ces *Kernels*. Mais, nous préférons grandement cela à l'alternative qui nous avions qui est de charger plus de valeur pour les *threads* qui sont sur les bordures d'un *block*. Parce que cela implique que nous aurions eux des *Kernels* qui ont un temps d'exécution plus grand que d'autres et cela aurait ralentis grandement notre algorithme. L'autre désavantage, est que nous aurions été obligé de faire plus qu'une lecture à la mémoire partagée pour ces *Kernels* ce qui est très mauvais pour la performance.

Finalement, pour réglé la question de l'écriture et de la lecture des valeurs dans la mémoire partagé globales nous avons besoin d'au minimum 2 matrices. La première est utilisé pour fournir la valeur source qui est utilisé par l'algorithme. La deuxième nous sert à écrire la réponse de chaques *threads* pour éviter d'écraser des valeurs qui n'ont pas encore été lus. Après, quand une itération à été completée les 2 matrices sont échangées. Alors, la matrice avec les valeurs source est la dernière dans laquelle nous avons écrit les résultats. Et la matrice de réception se fait écraser par une nouvelle matrice avec les nouvelles valeurs.

### Discussion

Nous avons le plus a gagner à retravailler notre division en *block* parce que nous ne prenons pas en compte plusieurs variantes matériel importantes. Cela comprend la taille des *Warp* pour la grandeur des *blocks*. Le nombre de *Streaming Multiprocessors* pour avoir une quantité de *blocks* divisible par celui-ci. Nous n'avons pas non plus fait attentions à la coalescence de mémoire durant notre allocation de mémoire pour nos pointeurs dans lesquels nous copions la matrice sur le *GPU*.

## Conclusion

Finalement, plusieurs amélioration sont possible pour mieux prendre avantage du matériel spécifiquement utilisé plustot que de prendre pour aquis certains paramêtres importants. Mais nous croyons avoirs utilisé efficacement le matériel que nous avions pour obtenir une grande amélioration dans le temps de calcules de l'algorithme.